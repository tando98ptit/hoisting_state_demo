package com.example.add_list_name

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.add_list_name.ui.theme.Add_list_nameTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainScreen()
        }
    }
}

@Composable
fun MainScreen() {
    val greetingListState = remember { mutableStateListOf("John", "Amanda") }
    val newNameStateContent = remember { mutableStateOf("") }
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Top App Bar") },
                backgroundColor = MaterialTheme.colors.primarySurface,
                contentColor = contentColorFor(backgroundColor = Color.Yellow)
            )
        },
        content = {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                GreetingList(
                    greetingListState,
                    { greetingListState.add(newNameStateContent.value) },
                    newNameStateContent.value,
                    { newName -> newNameStateContent.value = newName }
                )
            }
        }
    )

}

@Composable
fun GreetingList(
    namesList: List<String>,
    buttonClick: () -> Unit,
    textFieldValue: String,
    textFieldUpdate: (newName: String) -> Unit,
) {
    for (name in namesList) {
        Greeting(name)
    }

    TextField(
        value = textFieldValue,
        onValueChange = textFieldUpdate,
        label = { Text("Enter text") },
        maxLines = 2,
        modifier = Modifier.padding(20.dp)
    )

    Button(onClick = buttonClick) {
        Text(text = "Add new name")
    }
}


@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name",
        style = MaterialTheme.typography.h5,
        fontSize = 30.sp,
        fontStyle = FontStyle.Italic
    )
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MainScreen()
}